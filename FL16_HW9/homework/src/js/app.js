let userEnter = prompt('Enter name','meeting');
let values = document.querySelectorAll('input'),
    validateCheck = true,
    form = document.querySelector('form');
if(userEnter){
    form.style.display = 'block';
    let confirm = document.querySelector('.confirm'),
        converter = document.querySelector('.converter');
    confirm.addEventListener('click', validate);
    converter.addEventListener('click', calcConver);
}
function validate(){
    event.preventDefault();
    validateCheck = true;
    for (let i = 0; i < values.length; i++) {
        if(!values[i].value){
            validateCheck = false;
            alert(`Input all data`);
            break;
        }
    }
    if(!values[1].value.match(/^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/)){
        validateCheck = false;
        alert('Enter time in format hh:mm');
    }
    if(validateCheck){
        console.log(`${values[0].value} has a meeting today at ${values[1].value} somewhere in ${values[2].value}`);
    }
}
function calcConver() {
    event.preventDefault();
    let euro = prompt('Input amount of euro'),
        dol = prompt('Input amount of dollars'),
        dolToHrv = 27,
        euroToHrv = 33;
    if(euro>0 && dol>0){
        alert(`${euro} euros are equal ${(euro*euroToHrv).toFixed()} hrns,
${dol} dollars are equal ${(dol*dolToHrv).toFixed()} hrns`);
    }
}
