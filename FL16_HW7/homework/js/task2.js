let answer = confirm('Do you want to play a game?'),
    stage = 1,
    totalPrize = 0,
    prizeFirstAttempt = 100, deltaSecPrize = 50, deltaThirdPrize = 75;
if(answer){
    play()
} else if (!answer){
    alert('You did not become a billionaire, but can.');
}
function play(){
    let min = 0, max = 4 + stage*4, count = 3,
        num = Math.round(Math.random()*(max - min) + min),
        choice = prompt(question(min, max, count, totalPrize, stage*prizeFirstAttempt));
        function makeChocie(){
            if(choice === num) {
                let prize;
                if(count===3) { 
                    prize = prizeFirstAttempt; 
                } else if (count===2) { 
                    prize = prizeFirstAttempt - deltaSecPrize; 
                } else if (count===1) { 
                    prize = prizeFirstAttempt - deltaThirdPrize; 
                }
                answer = confirm(cong(stage, prize));
                totalPrize += stage*prize;
                if(answer){ 
                    stage++;
                    play();
                }
            } else{
                count--;
                if(count){
                    let possiblePrize;
                    if (count===3) { 
                        possiblePrize = prizeFirstAttempt; 
                    } else if (count===2) { 
                        possiblePrize = prizeFirstAttempt - deltaSecPrize;
                    } else if (count===1) { 
                        possiblePrize = prizeFirstAttempt - deltaThirdPrize;
                    }
                    choice = prompt(question(min, max, count, totalPrize, stage*possiblePrize));
                    makeChocie();
                } else{
                    alert(`Thank you for your participation. Your prize is: ${totalPrize}$`)
                    answer = confirm('Would you like to play again?');
                    if(answer){ 
                        stage = 1;
                        play();
                    }
                }
            }
        }
    makeChocie();
}
function question(min, max, count, totalPrize, prize){
    return `Choose a roulette pocket number from ${min} to ${max}\nAttempts left: ${count}
Total prize: ${totalPrize}\nPossible prize on current attempt: ${prize}`;
}
function cong(stage, prize){
    return `Congratulation, you won! Your prize is ${stage*prize}$. Do you want to continue?`;
}