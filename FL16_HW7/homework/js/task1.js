let money = +prompt(`Put initial amount of money`),
    yearsNum = +prompt(`Put number of years`),
    percentage = +prompt(`Put percentage of a year`);
if(typeof money === 'number' && money >= 1000 && typeof yearsNum === 'number' && 
    yearsNum >= 2 && Number.isInteger(yearsNum) && typeof percentage === 'number' && percentage <= 100){
    showProfit();
} else {
    alert('Invalid input data');
}
function showProfit(){
    let totProf = 0,
        totAm = money,
        resString = `Initial amount: ${money}\nNumber of years: ${yearsNum}\nPercentage of year: ${percentage}`;

    for (let i = 1; i <= yearsNum; i++) {
        let profit = totAm*percentage/100;
        totProf += profit;
        totAm += profit;
        if(i === 1){
            resString += `\n\n${i} Year\nTotal profit: ${totProf.toFixed(2)}(${percentage}% from initial amount)
Total amount: ${totAm.toFixed(2)}(initial amount + total profit)`;
        } else {
            resString += `\n\n${i} Year
Total profit: ${totProf.toFixed(2)}(previous profit + ${percentage}% from previous total amount (${totAm - profit}))
Total amount: ${totAm.toFixed(2)}(initial amount + total profit)`;
        }
    }
    alert(resString);
}