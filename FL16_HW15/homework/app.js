const root = document.getElementById('root');
let tweetItems = {
    'mainBlock': document.querySelector('#tweetItems'),
    'buttonGoToLike': createElementFunc('button', 'class', 'goLikeTweet', 'Go to liked'),
    'visibility': changeBlockVisibility
}
tweetItems.buttonGoToLike.classList.add('hidden');
document.querySelector('#navigationButtons').append(tweetItems.buttonGoToLike);
let addBlock = {
    'mainBlock': null,
    'header': null,
    'textarea': null,
    'blockFormButtons': null,
    'buttonCancel': null,
    'buttonSave': null,
    'visibility': changeBlockVisibility,
    setBlockEvents: setEvent
}
let editBlock = {
    'mainBlock': document.querySelector('#modifyItem'),
    'header': document.querySelector('#modifyItemHeader'),
    'textarea': document.querySelector('#modifyItemInput'),
    'blockFormButtons': document.querySelector('#modifyItem').querySelector('.formButtons'),
    'buttonCancel': document.querySelector('#cancelModification'),
    'buttonSave': document.querySelector('#saveModifiedItem'),
    'visibility': changeBlockVisibility,
    setBlockEvents: setEvent,
    currentEditTweet: null
}
let addedTweetsObj = {
    'mainBlock': createAddedTweetsBlock(),
    addedTweets: [],
    'visibility': changeBlockVisibility,
    setBlockEvents: setEvent
}
let likedTweetsBlock = {
    'mainBlock': createLikedTweetsBlock(),
    'visibility': changeBlockVisibility,
    likedTweets: getLikedTweets,
    showLikeTweets: function(){
        // console.log(likedTweetsBlock.mainBlock);
    }
}
tweetItems.buttonGoToLike.addEventListener('click', likedTweetsBlock.showLikeTweets);
function getLikedTweets(){
    let likedTweetsArr = addedTweetsObj.mainBlock.querySelectorAll('.like');
    return likedTweetsArr;
}
const addTweetBlockButton = root.querySelector('.addTweet');
addTweetBlockButton.addEventListener('click', createAddItem);
editBlock.setBlockEvents('buttonSave', 'click', saveEdittedTweet);

function setEvent(elem, eventName, func){
    this[elem].addEventListener(`${eventName}`, func);
}
function createAddItem(){  
    addBlock['mainBlock'] = createElementFunc('div', 'id', 'addItem');
    addBlock['header'] = createElementFunc('h1', 'id', 'addItemHeader', 'Add tweet');
    addBlock['textarea'] = createElementFunc('textarea', 'id', 'addItemInput');
    addBlock['blockFormButtons'] = createElementFunc('div', 'class', 'formButtons');
    addBlock['buttonCancel'] = createElementFunc('button', 'id', 'cancelAdding', 'Cancel');
    addBlock['buttonSave'] = createElementFunc('button', 'id', 'saveAddedItem', 'Save Changes');
    addBlock['blockFormButtons'].append(addBlock['buttonCancel'], addBlock['buttonSave']);
    addBlock['mainBlock'].append(addBlock['header'], addBlock['textarea'], addBlock['blockFormButtons']);
    root.prepend(addBlock.mainBlock);
    addBlock.setBlockEvents('buttonSave', 'click', () => { 
        saveTweet(addBlock)
    });
    addBlock.setBlockEvents('buttonCancel', 'click', cancelTweet);
    tweetItems.visibility(false);
    editBlock.visibility(false);
}
function createAddedTweetsBlock(){
    let addedTweetsBlock = createElementFunc('div', 'class', 'addedTweetsBlock');
    root.append(addedTweetsBlock);
    return addedTweetsBlock;
}
function createLikedTweetsBlock(){
    let likedTweetsBlock = createElementFunc('div', 'class', 'likedTweetsBlock');
    root.append(likedTweetsBlock);
    return likedTweetsBlock;
}
function createElementFunc(blockType, typeId, classIdName, innerText){
    let elem = document.createElement(blockType);
    if(typeId === 'id'){
        elem.setAttribute('id', classIdName);
    } else if(typeId === 'class'){
        elem.classList.add(classIdName);
    }
    if(innerText){
        elem.innerText = innerText;
    }
    return elem;
}
function saveTweet(blockCalled){    
    let textTweet = blockCalled['textarea'].value;
    let checked = addedTweetsObj['addedTweets'].includes(textTweet);
    if(checked){
        console.log(`Error! You can't tweet about that`);
        // showNotification();
    } else if(textTweet !== ''){
        addTweetToHTML(textTweet, addedTweetsObj['addedTweets'].push(textTweet));
    }
}
function addTweetToHTML(textTweet, index){
    let tweetItem = createElementFunc('div', 'class', 'tweetItem'),
        p = createElementFunc('p', 'class', 'addedTweet', textTweet),
        buttonRemove = createElementFunc('button', 'class', 'removeTweet', 'remove'),
        buttonLike = createElementFunc('button', 'class', 'likeTweet', 'like');
    tweetItem.setAttribute('id', index);
    p.addEventListener('click', editTweet);
    tweetItem.append(p, buttonRemove, buttonLike);
    addedTweetsObj['mainBlock'].append(tweetItem);
    buttonRemove.addEventListener('click', () => {
        removeTweet(tweetItem)
    });
    buttonLike.addEventListener('click', () => {
        likeTweet(tweetItem)
    });
}
function removeTweet(tweetItem){
    let innerHTML = tweetItem.querySelector('p').innerHTML,
        indexToDelete = addedTweetsObj.addedTweets.indexOf(innerHTML, 0);
    addedTweetsObj.addedTweets.splice(indexToDelete, 1);
    console.log(addedTweetsObj.addedTweets);
    tweetItem.remove();
}
function likeTweet(tweetItem){
    if(/\blike\b/.test(event.target.innerHTML)){
        console.log(`Hooray! You like tweet with id ${tweetItem.getAttribute('id')}!`);
        tweetItem.classList.add('like');
        event.target.innerHTML = 'unlike';
    } else if(/\bunlike\b/.test(event.target.innerHTML)) {
        console.log(`Sorry You no longer like tweet with id ${tweetItem.getAttribute('id')}!`);
        tweetItem.classList.remove('like');
        event.target.innerHTML = 'like';
    }
    if(likedTweetsBlock.likedTweets()){
        tweetItems.buttonGoToLike.classList.remove('hidden');
    } else {
        tweetItems.buttonGoToLike.classList.add('hidden');
    }
}
function cancelTweet(){
    addBlock.textarea.value = '';
}
function editTweet(){
    editBlock['textarea'].value = this.innerHTML;
    editBlock.currentEditTweet = this;
    addBlock.visibility(false);
    editBlock.visibility(true);
    addedTweetsObj.visibility(false);
}
function saveEdittedTweet(){
    let checked = addedTweetsObj['addedTweets'].includes(editBlock.textarea.value);
    if(checked){
        console.log(`Error! You can't tweet about that`);
        // showNotification();
    } else if(editBlock.textarea.value !== '') {
        addedTweetsObj.addedTweets.forEach((el, index) => {
            if(el === editBlock.currentEditTweet.innerText){
                addedTweetsObj.addedTweets[index] = editBlock.textarea.value;
                editBlock.currentEditTweet.innerText = editBlock.textarea.value;
            }
        })
        editBlock.currentEditTweet = null;
        tweetItems.visibility(true);
        editBlock.visibility(false);
        addedTweetsObj.visibility(true);
    }
}
function changeBlockVisibility(visibilBoollean){
    let onPage = !/hidden/.test(this.mainBlock.className);
    if(visibilBoollean && onPage){
        // nothing
    } else if(visibilBoollean && onPage === false){
        this.mainBlock.classList.remove('hidden');
    } else if(!visibilBoollean && onPage){
        this.mainBlock.classList.add('hidden');
    } else if(!visibilBoollean && onPage === false){
        // nothing
    }
}
function showNotification(){
    document.querySelector('#alertMessageText').innerText = "Error! You can't tweet about that";
}