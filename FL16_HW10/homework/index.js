function isEquals(num1, num2){
    return num1===num2;
}
isEquals(3, 3);
function isBigger(num1, num2){
    return num1 > num2;
}
isBigger(5, -1);
function storeNames(...args){
    return args;
}
storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy');
function getDifference(num1, num2){
    if(num1 >= num2){
        return num1 - num2;
    } else{
        return num2 - num1;
    }
}
getDifference(5, 3);
function negativeCount(arr){
    return arr.reduce((acc, el) => {
        if(el<0){
            acc++;
        } 
        return acc;
    },0)
}
negativeCount([4, 3, 2, 9]);
negativeCount([0, -3, 5, 7]);
function letterCount(word, symb){
    return word.split('').reduce((acc, el) => {
        if(el===symb){
            acc++;
        }
        return acc;
    }, 0)
}
letterCount('Marry', 'r');
letterCount('Barny', 'y');
letterCount('', 'z');
function countPoints(arr){
    return arr.reduce((acc, el) => {
        let matchResult = el.split(':');
        if(+matchResult[0]>+matchResult[1]){
            acc += 3;
        } else if(+matchResult[0]===+matchResult[1]){
            acc += 1;
        } 
        return acc;
    }, 0)
}
countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']);