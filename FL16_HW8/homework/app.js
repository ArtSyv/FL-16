function reverseNumber(num) {
    let resString='';
    num = num+'';
    for (let i = num.length-1; i >= 0; i--) {
        if(num[i]!=='-'){
            resString += num[i];
        }
    }
    if(num[0]==='-'){
        resString = `-${resString}`;
    }
    return resString;
}
reverseNumber(-156);
function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}
forEach([2,5,8], function(el) { 
    console.log(el);
});
function map(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        return func(arr[i]);
    }
}
map([2, 5, 8], function(el) {
    return el + 3;
});
map([1, 2, 3, 4, 5], function (el) {
    return el * 2;
});
function filter(arr, func) {
    let res = [];
    for (let i = 0; i < arr.length; i++) {
        if(func(arr[i])){
            res.push(arr[i]);
        }
    }
    return res;
}
filter([2, 5, 1, 3, 8, 6], function(el) {
    return el > 3;
})
filter([1, 4, 6, 7, 8, 10], function(el) {
    return el % 2 === 0 
})
function getAdultAppleLovers(data) {
    let res = [];
    for (let index = 0; index < data.length; index++) {
        if(data[index]['favoriteFruit'] === 'apple' && data[index]['age']>=18){
            res.push(data[index]['name']);
        }
    }
    return res;
}
let data = [
    {
        '_id': '5b5e3168c6bf40f2c1235cd6',
        'index': 0,
        'age': 39, 'eyeColor': 'green',
        'name': 'Stein',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e3168e328c0d72e4f27d8',
        'index': 1,
        'age': 38,
        'eyeColor': 'blue',
        'name': 'Cortez',
        'favoriteFruit': 'strawberry'
    },
    {
        '_id': '5b5e3168cc79132b631c666a',
        'index': 2,
        'age': 2,
        'eyeColor': 'blue',
        'name': 'Suzette',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e31682093adcc6cd0dde5',
        'index': 3,
        'age': 17,
        'eyeColor': 'green',
        'name': 'Weiss',
        'favoriteFruit': 'banana'
    }
]
getAdultAppleLovers(data)
function getKeys(obj) {
    let res = [];
    for(let key in obj){
        res.push(key);
    }
    return res;
}
getKeys({keyOne: 1, keyTwo: 2, keyThree: 3})
function getValues(obj) {
    let res = [];
    for(let key in obj){
        res.push(obj[key]);
    }
    return res;
}
getValues({keyOne: 1, keyTwo: 2, keyThree: 3})
function showFormattedDate(dateObj) {
    dateObj = ''+dateObj;
    let year = '', month = '', day = '';
    for (let i = 0; i < dateObj.length; i++) {
        if(i>=11 && i<=15){
            year += dateObj[i];
        } else if(i>=4 && i<=7){
            month += dateObj[i];
        } else if(i>=8 && i<=9){
            day += dateObj[i];
        }
    }
    return `It is ${day} of ${month}, ${year}`;
}
showFormattedDate(new Date('2018-08-27T01:10:00'))